<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    use HasFactory;
    
  
    protected $table  = 'registrations';
    protected $fillable = [
    'name',
    'lastnames',
    'type_doc',
    'doc_number',
    'email',
    ];

public const DOCUMENTO = ['DNI','PASAPORTE'];
public function type_doc(){
    return self::DOCUMENTO[ $this->type_doc];
}

   
}
