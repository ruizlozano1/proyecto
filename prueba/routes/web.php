<?php

use App\Http\Controllers\RegistrationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('registration', [RegistrationController::class, 'create'])->name('prueba.create');
Route::post('guardar', [RegistrationController::class, 'store'])->name('prueba.store');
Route::get('listar', [RegistrationController::class, 'index'])->name('prueba.index');