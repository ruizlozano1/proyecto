@extends('tema.app')
@section('title',"Listado")
@section('contenido')
<h3>Listado de Registro</h3>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>
                Nombre
            </th>
            <th>
                Apellidos
            </th>
            <th>
                Tipo Documento
            </th>
            <th>
                Numero docuemnto
            </th>
            <th>
                Email
            </th>
           
        </tr>
    </thead>
    <tbody>
        @foreach($registrations as $registration)
        <tr>
            <td>
                {{ $registration -> name}}
            </td>
            <td>
                {{ $registration -> lastnames}}
            </td>
            <td>
                {{ $registration -> type_doc()}}
            </td>
            <td>
                {{ $registration -> doc_number}}
            </td>
            <td>
                {{ $registration -> email}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection