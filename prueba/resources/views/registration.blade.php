@extends('tema.app')
@section('title',"Nuevo Registro")
@section('contenido')
<h3>
    Registrar
</h3>



<form action="{{route('prueba.store')}}" method="POST">
    @csrf
    <div class="row">

        <div class="col-sm-12">
            <label for="InputNombre" class="form-label">* Nombre</label>
            <input type="text" name="name" id="InputNombre" class="form-control" placeholder="..." value="{{ old('name') }} ">

            <label for="InputApellido" class="form-label">* Apellidos</label>
            <input type="text" name="lastnames" id="InputApellido" class="form-control" placeholder="..."  value="{{ old('lastnames') }} ">

            <label for="SelectDocumento" class="form-label">* Tipo Documento</label>
            <select name="type_doc" id="SelectDocumento" class="form-select">
                <option value="0">DNI</option>
                <option value="1">PASAPORTE</option>
            </select>

            <script>
                document.getElementById('SelectDocumento').value = "old('type_doc')";
            </script>

            
            <label for="InputNumDocumento" class="form-label">* Nº Documento</label>
            <input type="text" name="doc_number" id="InputNumDocumento" class="form-control" placeholder="..." value="{{ old('doc_number') }} ">

            <label for="InputEmail" class="form-label">* Email</label>
            <input type="email" name="email" id="InputEmail" class="form-control" placeholder="name@example.com" value="{{ old('email') }} ">

            <div class="col-sm-12 text-center my-2">
                <button type="submit" class="btn btn-primary">
                        Grabar
                </button>
            </div>
        </div>
    </div>

</form>

<h1>Create Post</h1>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@endsection